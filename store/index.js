import Vuex from 'vuex'

const store = () => new Vuex.Store({
  state: {
    authen: false
  },
  mutations: {
    setAuthen(state, status) {
      state.authen = status
    }
  },
  actions: {
    SET_AUTHEN({ commit }, status) {
      commit('setAuthen', status)
    }
  },
  getters: {
    GET_AUTHEN: state => {
      return state.authen
    }
  }
})

export default store
