const express = require('express')
const app = express()
const mongoose = require('mongoose')

mongoose
  .connect(
    'mongodb://list-todo:Camonem123@ds227674.mlab.com:27674/list-to-do',
    {
      useNewUrlParser: true
    }
  )
  .then(() => {
    console.log('Database connected')
  })
mongoose.set('useFindAndModify', false)

require('./tasks')
const controller = require('./controller')
app.use('/api', controller)

module.exports = {
  path: '/api',
  handler: controller
}