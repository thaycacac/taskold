const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const Task = mongoose.model('Task')

const app = express()
router.use((req, res, next) => {
  Object.setPrototypeOf(req, app.request)
  Object.setPrototypeOf(res, app.response)
  req.res = res
  res.req = req
  next()
})

router.post(`/create`, async (req, res) => {
  try {
    const { content, note, dealine, status } = req.body
    const newTask = await Task.create({
      content,
      note,
      dealine,
      status
    })
    await newTask.save()
    res.status(200).json({
      data: newTask
    })
  } catch (error) {
    console.log(error)
  }
})

router.get(`/getlist`, (req, res) => {
  Task.find()
    .then(result => {
      res.status(200).json({
        data: result
      })
    })
    .catch(error => {
      console.log(error)
    })
})

router.post(`/update`, async (req, res) => {
  const { id } = req.body
  try {
    await Task.findOneAndUpdate(
      { _id: id },
      {
        status: true
      }
    ).then(response => {
      res.status(200).json({
        data: `Success`
      })
    })
  } catch (error) {
    console.log(error)
  }
})

module.exports = router