const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Task = new Schema({
  content: {
    type: String,
    required: true
  },
  note: {
    type: String,
    required: true
  },
  dealine: {
    type: String,
    required: true
  },
  status: {
    type: Boolean,
    required: true
  }
})

module.exports = mongoose.model('Task', Task)